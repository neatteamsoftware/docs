# Vision

## What is Image Processing? 

It is a field in computer science, which deals with the automatic processing of images based on the real world, in order to extract and interpret visual information contained in them.

---

## Libraries We'll Be Using:

- OpenCV (Imported as cv2)
- numpy
- imutils (So far, we needed nothing but “grab_contours” , so we imported only this)

### Importing these libraries:
``` python
import cv2
import numpy as np
from imutils import grab_contours
```

---

## Basics:

### Overview
**How do we process an image from a video?** 
A video is a collection of images showing one after another. Each image is referred as a frame. 
When we process a video (Whether it's taken from the camera or from the file system) we process each frame as an independent image and then show those frames in a row just as the video. 

![](https://paper-attachments.dropbox.com/s_6B5554C30D923655E48D59BC8E4E0F7A2FB5282918890354676C217AFA4787C4_1599134185866_aZ4AKRos.jpeg)

**The processing step:**
What is a pipeline?
a pipeline is a set of data processing elements connected in series, where the output of one element is the input of the next one.
the processing step is a pipeline , combination of many many functions being executed one after another. 
for example:

![](https://paper-attachments.dropbox.com/s_6B5554C30D923655E48D59BC8E4E0F7A2FB5282918890354676C217AFA4787C4_1599137453450_rtyjfgnfytu.jpg)

> Each frame is a numpy array. What we see as a picture the computer sees it as numbers. 
> If we will print a small part of a black frame we will get something like this 
> [[[0 0 0]
>   [0 0 0]
>   [0 0 0]]
> 
>  [[0 0 0]
>   [0 0 0]
>   [0 0 0]]
> 
>  [[0 0 0]
>   [0 0 0]
>   [0 0 0]]]

### Video Capture

Video Capture: class for capturing videos from files, image sequences or cameras.  
Its constructors:

- `cv2.VideoCapture(file_name)`
- `cv2.VideoCapture(device)`

parameters:
- **file_name** - name of the opened video file (Path to the file if it isn't in the same folder as the code, but in an inner folder).
- **device** - id of the opened video capturing device (A camera index). If there is a single camera connected, just pass 0.

Useful methods:
- `isOpened()`  Returns true if video capturing has been initialized.
- `release()`  Closes the video file or the capturing device.
- `read()`  Grabs and returns the next video frame. Also returns retval (If succeeded - true else - false)

Examples:

1. captures 5 frames only
``` python
import cv2

# captures the 2nd camera connected
second_camera = cv2.VideoCapture(1)

frames_grabbed = 0  # a simple counter of frames since we want to show only 5 frames
while second_camera.isOpened():
    ret, frame = second_camera.read()

    if ret:
        print('I grabbed a frame from the camera! yay!')
        frames_grabbed += 1
    if frames_grabbed == 5: 
        break

second_camera.release() 
```

2. 
``` python
import cv2

# captures the video named 'myFavoriteVideo.mp4'
my_video = cv2.VideoCapture('inner_folder\myFavoriteVideo.mp4')

while my_video.isOpened():
    ret, frame = my_video.read()

    if ret:
        print('I grabbed a frame from my faforite video! woohoo!')

my_video.release() 
```

## Showing A Frame

The function `cv2.imshow('window_name', frame)`  puts the frame in the window given. You can create a window beforehand , or the function will create its own new window (If you’ll write a window_name that does not already exist , OpenCV will create that window)
How to create a window?
`cv2.namedWindow('window_name')`.

Example:
```python
import cv2

cap = cv2.VideoCapture(0)

while cap.isOpened():
    ret, frame = cap.read()

    cv2.imshow('new_window', frame)
    cv2.namedWindow('another_window')
    cv2.imshow('another_window', frame)

    key = cv2.waitKey(1)  # waits for a key to be clicked if clicked it stores its ascii
    if key == 32:  # space bar
        break

# closes all windows opened
cv2.destroyAllWindows()
cap.release()
```

## Changing Color Spaces

**What is a color space?**
A color space is a specific organization of color such as RGB, HSL, HSV.

Examples:
- RGB: red, green, blue
- BGR: blue, green, red
- HSV: hue, saturation, value
- CMYK: cyan, magenta, yellow, and black.

OpenCV uses BGR.

![HSV](https://paper-attachments.dropbox.com/s_6B5554C30D923655E48D59BC8E4E0F7A2FB5282918890354676C217AFA4787C4_1599144554506_HSV_color_solid_cylinder.png)

![RGB](https://paper-attachments.dropbox.com/s_6B5554C30D923655E48D59BC8E4E0F7A2FB5282918890354676C217AFA4787C4_1601577207617_s_6B5554C30D923655E48D59BC8E4E0F7A2FB5282918890354676C217AFA4787C4_1599144516498_download.png)

**But why BGR when the whole world uses RGB?**
The reason the early developers at OpenCV chose BGR color format is that back then BGR color format was popular among camera manufacturers and software providers.
BGR was a choice made for historical reasons and now we have to live with it.
BGR pixel format does not play well with libraries that use the standard RGB pixel format , the HSV format etc.

**How to change the color space ?**
There are more than 150 color-space conversion methods available in OpenCV. But we will look into only three which are most widely used ones.
BGR → Gray, BGR → HSV, BGR → RGB.
We use the function `cv2.cvtColor(input_image, flag)` to convert color spaces, where `flag` determines the type of conversion.

As mentioned above, there are many flags available. 
Here are the flags for the conversions discussed:

- For BGR → Gray use `cv2.COLOR_BGR2GRAY`.
- For BGR → HSV use `cv2.COLOR_BGR2HSV`.
- For BGR → RGB use `cv2.COLOR_BGR2RGB`.

To see all the flags available run the following script:

``` python
import cv2
flags = [i for i in dir(cv2) if i.startswith('COLOR_')]
print(flags)
```

## Filtering Image By Color Range

Filtering a color from an image is usually done with the following steps:
- Take each frame of the video
- Convert from BGR to HSV color-space
- Threshold the HSV image for a range of color
- Now extract the colored object alone, then, do whatever on that image you want.

Steps [1](#video-capture) & [2](#changing-color-spaces) are already explained above.

**Thresholding the  image for a range of color**
First, we need a range of colors
Lets say we want to extract the red color
The HSV range is something like [0, 50, 20] - [5, 255, 255]
we will create two numpy arrays , one the lower limit of the color
one the upper limit. 

``` python
lower_red = np.array([0, 50, 20])
upper_red = np.array([5, 255, 255])
```

Then, we will extract only the pixels that their color is in between these limits,
using the method `cv2.inRange(image, lower_bound, upper_bound)`.

This method returns the pixels that their colors match.
Those who match - in White, those who don’t match - in black.

Then, Bitwise-and mask (Returned from the inRange method) and original image
(`cv2.bitwise_and(src1,src2 ,mask(optional)`) - Calculates the per-element bit-wise conjunction of two arrays (Or an array and a scalar).
After that , show the resulted frame and that’s it

``` python
import cv2

cap = cv2.VideoCapture(0)
lower_red = np.array([0,50,20])
upper_red = np.array([5,255,255])

while cap.isOpened():
    _, frame = cap.read()

    hsv_image = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)  
    red_mask = cv2.inRange(hsv_image,lower_red,upper_red)
    result = cv2.bitwise_and(frame,frame,mask = red_mask)

    cv2.imshow('res',result)
    cv2.imshow('mask',red_mask)
    cv2.imshow('frame',frame)

    if cv2.waitKey(1) == 32:  # space bar
        break
cap.release()
```

![](https://paper-attachments.dropbox.com/s_6B5554C30D923655E48D59BC8E4E0F7A2FB5282918890354676C217AFA4787C4_1599663949411_8.png)



## Recognizing Shapes:
###  Finding and drawing contours

**what are contours?**
contours are simply a curve joining all the continuous points having same color or intensity. The contours are a useful tool for shape analysis and object detection and recognition.

For the best accuracy, use binary images. So before finding contours, apply threshold (Or canny edge detection).

**Threshold**
If pixel value is greater than a threshold value, it is assigned one value (May be white), else it is assigned another value (May be black). The function used is `cv2.threshold`.
 First argument is the source image, which should be a grayscale image. Second argument is the threshold value which is used to classify the pixel values. Third argument is the maxVal which represents the value to be given if pixel value is more than the threshold value.
OpenCV provides different styles of thresholding and it is decided by the fourth parameter of the function. Different types such as:

- cv2.THRESH_BINARY
- cv2.THRESH_BINARY_INV
- cv2.THRESH_TRUNC
- cv2.THRESH_TOZERO
![Different types of thresholds](https://paper-attachments.dropbox.com/s_6B5554C30D923655E48D59BC8E4E0F7A2FB5282918890354676C217AFA4787C4_1599997934345_threshold.jpg)


The `cv2.threshold(gray_img, val, max_val, type)` function returns two outputs, one - [retval](https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_imgproc/py_thresholding/py_thresholding.html#otsus-binarization) and the second one - thresholded image.

**Canny edge detection**
 `cv2.Canny(gray_img, min_val, max_val)`  receives an image in grayscale and outputs a black-and-white image, where the edges are marked in white and the rest of the image in black.
Simply put 'edge' is set to be a sharp transition between shades. 
Apart from the image itself the algorithm gets two other main parameters: upper threshold and lower threshold.


![thresholded cat (binary)](https://paper-attachments.dropbox.com/s_6B5554C30D923655E48D59BC8E4E0F7A2FB5282918890354676C217AFA4787C4_1599999865139_1.png)
![a random picture of a cat  😸](https://paper-attachments.dropbox.com/s_6B5554C30D923655E48D59BC8E4E0F7A2FB5282918890354676C217AFA4787C4_1599999709247_a_random_cat_drawing.png)
![edged cat](https://paper-attachments.dropbox.com/s_6B5554C30D923655E48D59BC8E4E0F7A2FB5282918890354676C217AFA4787C4_1599999898541_2.png)


**Finding contours**

> finding contours is like finding white object from black background. So remember, object to be found should be white and background should be black!

``` python
contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
```

 first argument is the source **binary** image, second is the contour retrieval mode, third is contour approximation method. And it outputs the image, contours and hierarchy (Depends on the OpenCv version , since OpenCV 3.2 `findContours()` no longer modifies the source image so an image is not returned). `contours` is a Python list of all the contours in the image. Each contour is a Numpy array of (x, y) coordinates of boundary points of the object.
 What is hierarchy?
 Sometimes contours are inside other contours. It’s like a parent contour that contains child contours.
Contour Retrieval Mode:
`cv2.RETR_LIST`
It simply retrieves all the contours, but doesn’t create any parent-child relationship. Parents and kids are equal under this rule, and they are just contours.
`cv2.RETR_EXTERNAL`
It returns only extreme outer flags. All child contours are left behind. We can say, under this law, Only the eldest in every family is taken care of.
`cv2.RETR_CCOMP`
This one is a little more complicated to understand than others, [you can read more about it in this link](https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_imgproc/py_contours/py_contours_hierarchy/py_contours_hierarchy.html#retr-ccomp).
It returns all the contours and arranges them to a 2 level hierarchy. External contours of the object are placed in hierarchy 1. And the contours of holes inside object (If any) are placed in hierarchy 2. If any object inside it, its contour is placed again in hierarchy 1. And its hole in hierarchy 2 and so on.
`cv2.RETR_TREE`
That is the most perfect one. It returns all the contours and creates a full family hierarchy list.


![A funny example with the Kardashian - Jenner family for contour retrieval modes 1&2&4🤣](https://paper-attachments.dropbox.com/s_6B5554C30D923655E48D59BC8E4E0F7A2FB5282918890354676C217AFA4787C4_1600027169407_1.png)


 
 Contour approximation method:
 As said before, contours are the boundaries of a shape with same intensity. It stores the (x, y) coordinates of the boundary of a shape. But does it store all the coordinates ? That is specified by this contour approximation method.
If you use  `cv2.CHAIN_APPROX_NONE`, all the boundary points are stored. But actually do we need all the points? For example, you found the contour of a line. you need just two points of that line. This is what `cv2.CHAIN_APPROX_SIMPLE` does. It removes all unnecessary points, and compresses the contour, and saving memory.

**Drawing contours**
`cv2.drawContours(img,contours, contour_index, color, thickness)`

1st argument - img to draw on
2nd argument - the contours (Should be passed as a python list)
3rd argument - contour index, useful if you want to draw a single contour. to draw all pass -1
4th & 5th argument - color & thickness

**convex hull**

![Convex-Concave](https://www.learnopencv.com/wp-content/uploads/2018/08/Convex-Concave.png)


A Convex object is one with no interior angles greater than 180. A shape that is not convex is called Non-Convex or Concave.
Hull means the exterior or the shape of the object.
Convex Hull of a group of points is a tight fitting convex boundary around the points of the shape.
`cv2.convexHull(a_contour)`
To draw the convexHull use `cv2.drawContours()`
since a convex is just a contour

![Concave - Convex](https://paper-attachments.dropbox.com/s_6B5554C30D923655E48D59BC8E4E0F7A2FB5282918890354676C217AFA4787C4_1600372860475_convexHull.png)


**Let's try to find the convex of this cute unicorn**

![UNICORN!!!!](https://paper-attachments.dropbox.com/s_6B5554C30D923655E48D59BC8E4E0F7A2FB5282918890354676C217AFA4787C4_1600374052993_toconvex.png)

``` python
import cv2
import numpy as np

img = cv2.imread('unicorn.png') 

hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
gray = cv2.cvtColor(hsv, cv2.COLOR_BGR2GRAY)

edges = cv2.Canny(gray, 100, 200)
contours, hir = cv2.findContours(edges, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
shape = cv2.convexHull(max(contours, key=cv2.contourArea))  # max cont
cv2.drawContours(img, [shape], -1, (0, 255, 255), 3)
```

![convexed UNICORN!!!!!](https://paper-attachments.dropbox.com/s_6B5554C30D923655E48D59BC8E4E0F7A2FB5282918890354676C217AFA4787C4_1600375692558_image.png)

## Denoising

Denoising a picture is probably the most important part of object detection, since it can change absolutely everything. 

### Blur

In OpenCV blurring removes high frequency content such as noise, edges (Well, there are blurring techniques which do not blur edges, but we will not cover them).
This line does [Gaussian blur](https://en.wikipedia.org/wiki/Gaussian_blur) : `blurred_img = cv2.GaussianBlur(img, (5, 5), 0)`.
We should specify the width and height of the kernel* which should be positive and odd (2nd parameter).
 

-  A kernel is a matrix that slides on each pixel and frames a group of pixels around it (the amount of pixels it surrounds depends on the size of the kernel in pixels, for example a 2x2 kernel surrounds a group of 4 pixels including the pixel it’s on). the kernel usually performs an action on the pixel it’s on (some sort of calculation) using the other pixels values in that kernel.
![original image -blurred image](https://paper-attachments.dropbox.com/s_6B5554C30D923655E48D59BC8E4E0F7A2FB5282918890354676C217AFA4787C4_1601040705998_scratch.png)

### Morphological Transformations

We will usually want to denoise a frame after the threshold, a good way to do that is to use `cv2.erode(threshed_frame, kernel)` and then `cv2.dilate(threshed_frame, kernel)`.

**Erode:**
In this function , a kernel is moving on the picture and changes every pixel in this kernel to the value of the minimal pixel in the kernel.
Since this function receives a thresholded frame,  a binary image with only 0 and 1 as the pixel values, if there is a 0 pixel (Black) in the kernel and that kernel is now on a 1 pixel (White) the white pixel will turn black (1 → 0)

![Erode gif. It’s long but watch until the end. The kernel moves on the picture and states which pixels will be changed](https://1.bp.blogspot.com/-k4txI837nGs/WCUmufWghJI/AAAAAAAAIv4/zEnTJY8ZV3ce99sStGUaNZMqFn_S4iXnQCK4B/s400/eroision.gif)


**Dilate:**
It’s the exact opposite of erode. every pixel changes to the maximal value (0 → 1, black to white)

**So why use this?**
Erode will narrow down the noise but won’t change the main shape. The main shape will return to itself with Dilate.

We can use this two functions one after each other, or we can use
  `cv2.morphologyEX(img, cv2.MORPH_OPEN, kernel)` (Opening), which is just another name for erosion followed by dilation.

**closing:**
It’s the opposite of opening, it's dilation followed by erosion. It’s useful for closing small holes inside the object. `cv2.morphologyEX(img, cv2.MORPH_CLOSE, kernel)`

## Calculations:
### Center of contour

Each shape is made of pixels, and the centroid is the weighted average of all the pixel.
Image Moment is a weighted average of image pixel intensities; To find the centroid of the image, we convert it to binary format and then find its center.

``` python
def findCenter(self, target):
    m = cv2.moments(target)
    cx = np.int(m['m10'] / m['m00'])
    cy = np.int(m['m01'] / m['m00'])
    return cx, cy
```

I will not explain the math behind this. [Here is Wikipedia’s explanation](https://en.wikipedia.org/wiki/Image_moment) about moments . 

### Angle

Here is the formula for **horizontal** angle of a point
`angle = (1 - 2 * x_point / camera_frame_width) * kCameraFovHorizontal / (2 * fov)` - field of view, a constant of the camera.
Imagine the frame was splitted in the center (a vertical line crossing the frame at the middle x), that is the 0 point.
Towards one side - the angle will be positive, towards the other - negative.
At the edges the angle will be `kCameraFovHorizontal / 2` and -`kCameraFovHorizontal / 2`.

## Others:
### Drawing
- thickness-  If **-1** is passed for closed figures like circles, it will fill the shape. *default thickness = 1*

**Put text on window:**
There’s not much to explain… 
`cv2.putText(frame_name, text, start at (x, y),` [font](https://github.com/opencv/opencv/blob/95fd61c9b4c2cbe099f84740c35587e53d55b6bb/modules/imgproc/include/opencv2/imgproc.hpp#L817)`, size, color, thickness)`

Example:
`cv2.putText(img, 'hello', (10, 150), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 1)`

**Draw circle:**
`cv2.circle(frame, (center_x, center_y), radius, color, thickness)`

Example:
`cv2.circle(img, (cx, cy), 10, (200, 200, 0), -1)`

**Draw rectangle:**
To draw a rectangle, you need the top-left corner and bottom-right corner of the rectangle.
 `cv2.rectangle(frame, top_left, bottom_right, color, thickness)`

Example:
 `cv2.rectangle(img, (384, 0), (510, 128), (0, 255, 0), 3)`
 

### Trackbars
Trackbars are very important because when we want to let the user of our program set certain values It's the easiest way to do that. 
All trackbars start at 0 and each position is an int (steps by  1).
Trackbars adjust their size to the window's size and they will always be on its top. 
On change event :
When the user moves the trackbar's position. 
You can `pass` it but you can also do something like print the post or store it somewhere. 

**Create a trackbar:**
`cv2.createTrackbar(tb_name,  self.win_name, val, max_val, on_change)`


- tb_name will be written next to the track bar 
- win_name- pass an existing window name. 
- val- default value- almost always 0
- max_value- the maximal value
- on_change- a method you create that receives and int and does something with it. If you want nothing to happen, pass nothing

**Set a trackbar position:**
`cv2.setTrackbarPos(tb_name, win_name, pos)`

**Get trackbar position (current value):**
`pos = cv2.getTrackbarPos(tb_name, win_name)`

**You can create your own trackbar, but here is a class that does this for you:**
on_change- If you want something to happen pass a string of code. 
``` python
class TB:  # track bars
    def __init__(self, name, w_name, max_val, on_change='pass', init_pos=0):
        # wName - window name, onChange - code to execute on change of tb pos
        self.name = name
        self.w_name = w_name

        self.val = 0
        self.max_val = max_val
        self.on_change = on_change

        # create trackbar
        cv2.createTrackbar(self.name, self.w_name, self.val, self.max_val, self.on_change)
        self.set_pos(init_pos)

    # Change value event
    # execute on change of tb
    def on_change(self, val):
        self.val = val
        exec(self.on_change)

    def get_pos(self):
        return self.val

    def set_pos(self, pos):
        cv2.setTrackbarPos(self.name, self.w_name, pos)
```
