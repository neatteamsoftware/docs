.. NeatTeam Docs documentation master file, created by
   sphinx-quickstart on Thu Oct 15 23:19:44 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to NeatTeam's documentation!
=========================================

.. toctree::
   :maxdepth: 1
   :caption: Docs:
   
   vision.md
